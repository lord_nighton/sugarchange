<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true" %>
<%@page import="com.sugar.enums.Errors"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change Sugars Here - Error page</title>
    </head>
    <body>
        <center>
            <div style='width : 100%; height : 300px; background-color : #E9967A; border : 1px solid black'>
                <table style='height : 300px; width : 100%'>
                    <tr>
                        <td style='font-size : 40px; height : 300px; text-align : center; vertical-align : middle'>
                            <c:choose>
                                <c:when test="${pageContext.exception == null}">
                                    <h3><%= Errors.DIRECT_ACCESS %></h3>
                                </c:when>
                                <c:otherwise>
                                    <h3><c:out value="${pageContext.exception.getMessage()}" /></h3>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </table>
            </div>
        </center>
    </body>
</html>