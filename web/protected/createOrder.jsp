<%@page import="java.util.ArrayList"%>
<%@page import="java.io.File"%>
<%@page import="com.sugar.galery.RandomGaleryImagesFetcher"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String title = "Change Sugar Here - confirm your order"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
<script type="text/javascript">
            $(document).ready ( function() {
                $("#loginText").css ("border", "1px solid #999");
                $("#passwordText").css ("border", "1px solid #999");
                
                $(window).resize(function(){
                    placeFooter();
                });
                placeFooter();
                $('#footer').css('display','inline');
                
                function placeFooter() {    
                    var windHeight = $(window).height();
                    var footerHeight = $('#footer').height();
                    var offset = parseInt(windHeight) - parseInt(footerHeight);
                    $('#footer').css('top',offset);
                }
                
                $("#submit").on ('click', function (e) {
                    if (0 === $("#indexCode").val().length || 
                        0 === $("#city").val().length || 
                        0 === $("#country").val().length ||
                        0 === $("#street").val().length ||
                        0 === $("#homeNumber").val().length ||
                        0 === $("#flatNumber").val().length ) {
                        alert ('Fill all required fields, please');
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
        <center>
            <div id='caption'>
                Confirm <font color='#C0C0C0'>your</font> order
            </div>
            <br />
            <div style='border : 2px dashed #999; padding-bottom : 10px; width : 900px;'>
                <table>
                    <tr>
                        <td style='text-align : right'>The sender :</td>
                        <td>${param.senderLogin}</td>
                    </tr>
                    <tr>
                        <td style='text-align : right'>Sugar :</td>
                        <td><img style='width : 150px; height : 150px;' src='../sugars/${param.senderLogin}/${param.sugar}' /></td>
                    </tr>
                    <tr>
                        <td style='text-align : right'>E-mail :</td>
                        <td><input type='text' value='${currentUser.mail}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right'>* Index code :</td>
                        <td><input id='indexCode' type='text' disabled value='${currentUser.address.indexCode}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right'>* Country :</td>
                        <td><input id='country' type='text' disabled value='${currentUser.address.country}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right'>* City :</td>
                        <td><input id='city' type='text' disabled value='${currentUser.address.city}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right'>* Street / avenue / square :</td>
                        <td><input id='street' disabled type='text' value='${currentUser.address.street}'></td>
                    </tr>
                    
                    <tr>
                        <td style='text-align : right'>* Home number :</td>
                        <td><input id='homeNumber' type='text' disabled value='${currentUser.address.homeNumber}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right'>* Flat number :</td>
                        <td><input id='flatNumber' type='text' disabled value='${currentUser.address.flatNumber}'></td>
                    </tr>
                </table>
                <form method='post' action='../CreateOrderServlet'>
                    <div style='visibility : hidden; display : none;'>
                        <input name='senderLogin' type='text' value='${param.senderLogin}' />
                        <input name='receiverLogin' type='text' value='${param.receiverLogin}' />
                        <input name='sugar' type='text' value='${param.sugar}' />
                    </div>
                    <input id='submit' type='submit' value='Confirm' />
                    <a style='font-size : 14px; color : #999; text-decoration : none;' href='modifyCredentials.jsp'>Modify credentials</a>
                </form>
            </div>
        </center>
    </body>
</html>