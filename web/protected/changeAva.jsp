<%@page import="com.sugar.users.User"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.File"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String title = "Change Sugar Here - change ava"; %>
<%@include file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript" src="../fancy/jquery.fancybox.js?v=2.1.4"></script>
	<link rel="stylesheet" type="text/css" href="../fancy/jquery.fancybox.css?v=2.1.4" media="screen" />
    </head>
    <body>
        <div style='position : absolute; float : left'>
            <img id='backButton' style='width : 50px; border : 2px dashed #AAA; padding : 15px' src='../images/back.png'>
        </div>
        <center>
            <span class="big">Choose new ava</span><br /><br />
            <div id="regform" class='littlegreytext'>
                <form action="../ChangeAvaServlet" method="post" enctype="multipart/form-data">
                    <input type="file" name="file" id="file" accept="image/x-png, image/gif, image/jpeg" />
                    <br /><br />
                    <input id="submitButton" type="submit" value="Change ava" />
                    <input id="clearButton" type="reset" value="Clear" />
                </form>
            </div>
        </center>