<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String title = "Change Sugar Here - modify creadentials"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
    </head>
    <body>
        <center>
        <div id='caption'>
            Modify <font color='#C0C0C0'>your</font> credentials
        </div>
        <form action='../ModifyUserCredentialsServlet' method='post'>
            <div style='border : 2px dashed #999; width : 900px; text-align : center; padding-bottom : 15px; margin-top : 20px'>
                <table style='width : 900px'>
                    <tr>
                        <td style='text-align : right; width : 450px'>Login :</td>
                        <td style='text-align : left; width : 450px'>
                            ${currentUser.login}
                            <input name='userLogin' style='visibility : hidden; display : none;' type='text' value='${currentUser.login}' />
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align : right; width : 450px'>E-mail : </td>
                        <td style='text-align : left; width : 450px'><input name='mail' type='text' value='${currentUser.mail}' /></td>
                    </tr>
                    <tr>
                        <td style='text-align : right; width : 450px'>Index code :</td>
                        <td style='text-align : left; width : 450px'><input name='indexCode' type='text' value='${currentUser.address.indexCode}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right; width : 450px'>Country :</td>
                        <td style='text-align : left; width : 450px'><input name='country' type='text' value='${currentUser.address.country}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right; width : 450px'>City :</td>
                        <td style='text-align : left; width : 450px'><input name='city' type='text' value='${currentUser.address.city}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right; width : 450px'>Street / avenue / square :</td>
                        <td style='text-align : left; width : 450px'><input name='street' type='text' value='${currentUser.address.street}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right; width : 450px'>Home number :</td>
                        <td style='text-align : left; width : 450px'><input name='homeNumber' type='text' value='${currentUser.address.homeNumber}'></td>
                    </tr>
                    <tr>
                        <td style='text-align : right; width : 450px'>Flat number :</td>
                        <td style='text-align : left; width : 450px'><input name='flatNumber' type='text' value='${currentUser.address.flatNumber}'></td>
                    </tr>
                </table>
                <input type='submit' value='Modify' />
                <input type='reset' value='Revert' />
            </div>
        </form>
        </center>
    </body>
</html>