<%@page import="java.util.ArrayList"%>
<%@page import="java.io.File"%>
<%@page import="com.sugar.galery.RandomGaleryImagesFetcher"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String title = "Change Sugar Here - Show sugars statistics"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript">
            $(document).ready ( function() {
                $("#allSugars").on ("click", function (e) {
                    $(this).css ("font-weight", "bold");
                    $("#allOrders").css("font-weight", "normal");
                    
                    $("#allSugarsDiv").css ("visibility", "visible").css ("display", "block");
                    $("#allOrdersDiv").css ("visibility", "hidden").css ("display", "none");
                    
                    $.ajax ({  
                        type : "GET",  
                        url : "../AjaxDisplayAllSugarsServlet", 
                        data : {    
                            pageContains : $("#pageContains").val(),
                            pageNumber : $("#pageNumber").val()
                        },
                        success : function (result) {   
                           $("#allSugarsDiv").html (result);
                        }                
                    });
                });
                
                $("#allOrders").on ("click", function (e) {
                    $(this).css ("font-weight", "bold"); 
                    $("#allSugars").css ("font-weight", "normal");
                    
                    $("#allSugarsDiv").css ("visibility", "hidden").css ("display", "none");
                    $("#allOrdersDiv").css ("visibility", "visible").css ("display", "block");
                    
                    $.ajax ({  
                        type : "GET",  
                        url : "../AjaxDisplayAllOrdersServlet", 
                        data : {},
                        success : function (result) {   
                           $("#allOrdersDiv").html (result);
                        }                
                    });
                });
                
                $("#allSugars").click();
                $("#fivePerPage").css("font-weight","bold").css("text-decoration","underline");
                
                $("#onePerPage").on ("click", function (e) {
                    $(this).css("font-weight","bold").css("text-decoration","underline");
                    
                    $("#fivePerPage").css("font-weight","normal").css("text-decoration","none");
                    $("#tenPerPage").css("font-weight","normal").css("text-decoration","none");
                    
                    $("#pageContains").val ("1");
                    $("#pageNumber").val ("0");
                    
                    $("#allSugars").click();
                });
                $("#fivePerPage").on ("click", function (e) {
                    $(this).css("font-weight","bold").css("text-decoration","underline");
                    
                    $("#onePerPage").css("font-weight","normal").css("text-decoration","none");
                    $("#tenPerPage").css("font-weight","normal").css("text-decoration","none");
                    
                    $("#pageContains").val ("5");
                    $("#pageNumber").val ("0");
                    
                    $("#allSugars").click();
                });
                $("#tenPerPage").on ("click", function (e) {
                    $(this).css("font-weight","bold").css("text-decoration","underline");
                    
                    $("#onePerPage").css("font-weight","normal").css("text-decoration","none");
                    $("#fivePerPage").css("font-weight","normal").css("text-decoration","none");
                    
                    $("#pageContains").val ("10");
                    $("#pageNumber").val ("0");
                    
                    $("#allSugars").click();
                });
                
                $("span[id^='page']").live ('click', function (e) {
                    var pageNumber = parseInt ($(this).text());
                    $("#pageNumber").val (pageNumber-1);
                    
                    $("#allSugars").click();
                }).live ('mouseover', function (e) {
                    $("span[id^='page']").css ("font-weight","normal").css ("text-decoration","none");
                    $(this).css ("font-weight","bold").css ("text-decoration","underline").css ("cursor", "pointer");
                }).live ('mouseout', function (e) {
                    $("span[id^='page']").css ("font-weight","normal").css ("text-decoration","none");
                }); 
                
                $("img[id^='ordableSugar']").live ('click', function (e) {
                    var sugarPath = $(this).attr ("src");
                    
                    var senderNameAndSugar = sugarPath.substring ("../sugars/".length);
                    var tokens = senderNameAndSugar.split ("/");

                    $("#senderLogin").val (tokens[0]);
                    $("#sugar").val (tokens[1]);

                    $("#createOrder").click();
                });
                
                $("span[id^='ordableSugarLink']").live ('click', function (e) {
                    var number = $(this).attr('id').substring ("ordableSugarLink".length);
                    var imageId = 'ordableSugar' + number;
                    $("#" + imageId).click();
                });
            });
        </script>
    </head>
    <body>
        <center>
            <br />
            Page contains<br /><br />
                <span style='cursor : pointer' id='onePerPage'>1</span>&nbsp;&nbsp;
                <span style='cursor : pointer' id='fivePerPage'>5</span>&nbsp;&nbsp;
                <span style='cursor : pointer' id='tenPerPage'>10</span>
            <br />
            <table style='width : 900px'>
                <tr>
                    <td style='width : 450px; text-align : center'>
                        <span id='allSugars' style='font-weight : bold'>Available sugars</span>
                    </td>
                    <td style='width : 450px; text-align : center'><span id='allOrders'>All orders</span></td>
                </tr>
            </table>

            <div id='allSugarsDiv' style='margin-bottom : 50px; padding-top : 15px; padding-left : 15px; padding-bottom : 4px; padding-right : 15px; width : 900px; border : 2px dashed #999; text-align : left'></div>

            <div id='allOrdersDiv' style='padding-top : 15px; padding-bottom : 10px; border : 2px dashed #999; width : 900px; visibility : hidden'></div>
        </center>
        <div style='display : none; visibility : hidden'>
            <input id='pageContains' type='text' value='5' />
            <input id='pageNumber' type='text' value='0'>
        </div>
        <div style='display : none; visibility : hidden'>
            <form method='post' action='createOrder.jsp'>
                <input id='receiverLogin' name='receiverLogin' type='text' value='${user.login}' />
                <input id='senderLogin' name='senderLogin' type='text' value='' />
                <input id='sugar' name='sugar' type='text' value='' />
                <input id='createOrder' type='submit' value='Order' />
            </form>
        </div>