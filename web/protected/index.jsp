<%@page import="com.sugar.users.UserImpl"%>
<%@page import="java.io.File"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sugar.galery.RandomGaleryImagesFetcher"%>
<%@page import="com.sugar.utils.UserUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sugar" uri="/WEB-INF/tlds/sugar_tags" %>
<% String title = "Change Sugar Here - main page"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript">
            $(document).ready ( function() {
                $("#loginText").css("border", "1px solid #999");
                $("#passwordText").css("border", "1px solid #999");
                
                $("#refreshButton").on ('click', function (e) {
                    window.location.reload (true);
                });
                
                $("#clearButton").on('click', function (e) {
                    $("#loginText").css("border", "1px solid #999");
                    $("#passwordText").css("border", "1px solid #999");
                });
                
                $("#submitButton").on('click', function (e) {
                    var loginLength = $("#loginText").val().length;
                    var passwordLength = $("#passwordText").val().length;
                    
                    if (loginLength === 0 && passwordLength === 0) {
                        $("#loginText").css("border", "1px solid red");
                        $("#passwordText").css("border", "1px solid red");
                        
                        return false;
                    }
                    
                    if (loginLength !== 0 && passwordLength === 0) {
                        $("#loginText").css("border", "1px solid #999");
                        $("#passwordText").css("border", "1px solid red");
                        
                        return false;
                    }
                    
                    if (loginLength === 0 && passwordLength !== 0) {
                        $("#loginText").css("border", "1px solid red");
                        $("#passwordText").css("border", "1px solid #999");
                        
                        return false;
                    }
                });
                
                $("img[id^='ordableSugar']").on ('click', function (e) {
                    var sugarPath = $(this).attr ("src");
                    
                    var userLogin = $("#currentUserLogin").val();
                    var userLoginLength = userLogin.length;
                    var adminUserLogin = $("#adminUserLogin").val();
                    
                    if (sugarPath.indexOf ("nosugars.png") === -1 && userLoginLength !== 0 && userLogin !== adminUserLogin) {
                        var senderNameAndSugar = sugarPath.substring ("../sugars/".length);
                        var tokens = senderNameAndSugar.split ("/");

                        $("#senderLogin").val (tokens[0]);
                        $("#sugar").val (tokens[1]);
                        
                        var receiverLogin = $("#currentUserLogin").val();
                        $("#receiverLogin").val (receiverLogin);
                        
                        $("#createOrder").click();
                    }
                });
                
                $("input[id^='sender']").on ('click', function (e) {
                    var number = $(this).attr ('id').substring ("sender".length);
                    $.ajax ({  
                        type : "GET",  
                        url : "../AjaxSendSugarServlet", 
                        data : {    
                            src : $("#sugar" + number).attr ("src")
                        },
                        success : function (result) { 
                            location.reload();
                        }                
                    });
                });
            });
        </script>
    </head>
    <body>
<%!
        private boolean isAvatarSetByExtensionExist (String login, String extension) {
            try {
                String avaPath = "avatars/" + login + "." + extension;
                String realFilePath = getServletContext().getRealPath (avaPath);
                File avatarFile = new File (realFilePath).getCanonicalFile();
                
                return avatarFile.exists();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    
        private String constructAvatarPath (String login) {
            try {
                if (true == isAvatarSetByExtensionExist (login, "jpeg")) {
                    return "avatars/" + login + ".jpeg";
                }
                if (true == isAvatarSetByExtensionExist (login, "jpg")) {
                    return "avatars/" + login + ".jpg";
                } 
                if (true == isAvatarSetByExtensionExist (login, "gif")) {
                    return "avatars/" + login + ".gif";
                }
                if (true == isAvatarSetByExtensionExist (login, "png")) {
                    return "avatars/" + login + ".png";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        
        private boolean isCurrentUserAdmin (UserImpl currentUser) {
            return UserUtils.isAdmin (currentUser.getLogin(), currentUser.getPassword());
        }
%>
        <center>
            <div id='caption'>
                Change<font color='#C0C0C0'> Sugars</font> Here
            </div>
            <div class='littlegreytext'>
                <br />
                sugar exchange is so simple...
            </div>
            
            <div id='container'>
                <div style='height : 20px'>&nbsp;</div>
                <div id='login' class='littlegreytext'>
<% 
                    if (true == currentUser.isUserUnloginned ()) {
%>
                        <form action='../LoginServlet' method='post'>
                            Login <br />
                            <input id='loginText' style='margin-top : 5px; margin-bottom : 10px;' maxlength='10' type='text' name='login' value=''/><br />
                            Password <br />
                            <input id='passwordText' style='margin-top : 5px; margin-bottom : 10px;' maxlength='10' type='password' name='password' value=''/><br />
                            <input id='submitButton' style='margin-top : 5px; margin-bottom : 5px;' type='submit' value='Login'>
                            <input id='clearButton' type="reset" value="Clear">
                            <br />
                            <a class='littlegreylink' href='registration.jsp'>Register now</a>
                        </form>
<%
                    } else {
                        if (true == isCurrentUserAdmin (currentUser)) {
%>                       
                            <a class='littlegreylink' href='adminPanel.jsp'>Admin panel</a>
                            <br />
                            <img src='../avatars/adminAva.png' style='width : 120px; height : 120px; border : 1px dashed #999;'/>
                            <br />
<%                          
                        } else {
                            String login = currentUser.getLogin();
                            String avatarPath = constructAvatarPath (login);
                            if (null == avatarPath) { %>
                                <c:out value="Hi, ${currentUser.login}"/>
                                <br />
                                <img src='../avatars/noAva.png' style='width : 120px; height : 120px; border : 1px dashed #999;' />
<%
                            } else {                           
%>
                                <c:out value="Hi, ${currentUser.login}"/>
                                <br />
                                <img src='../<%= avatarPath%>' style='width : 120px; height : 120px; border : 1px dashed #999;' />                                
<%
                            }
                        }
%>
                        <form action="../LogoutServlet" method="post">
                            <input type="submit" value="Logout">
                        </form>
<%                      
                    }
%>
                </div>
                <div style='float: left; width : 10px;'>&nbsp;</div>
                <div id='randomgalery'>
                    <c:forEach begin="0" end="4" step="1">
                        <c:choose>
                            <c:when test="${true == currentUser.login.isEmpty()}">
<%
                                String imagePathVisit = RandomGaleryImagesFetcher.defineRandomImagePathForVisitor (getServletContext());                        
%>
                                <img id="ordableSugar" src="<%=imagePathVisit%>" style="border : 1px dotted #AAA; width : 140px; height : 140px" />
                            </c:when>
                            <c:otherwise>
<%
                                String imagePath = RandomGaleryImagesFetcher.defineRandomImagePathForLoginnedUser (getServletContext(), currentUser.getLogin());                        
%>
                                <img id="ordableSugar" src="<%=imagePath%>" style="border : 1px dotted #AAA; width : 140px; height : 140px" />
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <div>
                        <%
                            boolean isCurrentUserAdmin = isCurrentUserAdmin (currentUser);
                            pageContext.setAttribute ("isCurrentUserAdmin", isCurrentUserAdmin);
                        %>
                        <c:choose> 
                            <c:when test="${false == isCurrentUserAdmin}">
                                <c:choose> 
                                    <c:when test="${false == currentUser.login.isEmpty()}">
                                        <div style='color : #999; font-size : 12px'>(click the sugar to order)</div>
                                        <img id="refreshButton" style='margin-top : 3px; margin-bottom : 0px; width : 20px' src='../images/refresh.png' />
                                    </c:when>
                                    <c:otherwise>
                                        <img id="refreshButton" style='margin-top : 10px; margin-bottom : 0px; width : 20px' src='../images/refresh.png' />
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <img id="refreshButton" style='margin-top : 10px; margin-bottom : 0px; width : 20px' src='../images/refresh.png' />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div style='margin : 2px; float: left; width : 1000px; height : 5px;'>&nbsp;</div>
<% 
                if (false == currentUser.isUserUnloginned() && false == isCurrentUserAdmin (currentUser)) {
%>
                    <div style='width : 985px; border : 2px dashed #AAA; float : left'>    
                        <div style='margin : 5px; text-align : center;'>
                            <a class='littlegreylink' href='cabinet.jsp'>Sugars control</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a class='littlegreylink' href='diagrams.jsp'>View "the most"</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a class='littlegreylink' href='modifyCredentials.jsp'>My credentials</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a class='littlegreylink' href='changeAva.jsp'>Change ava</a>
                        </div>
                    </div>
<%
                }
%>
            </div>
<%
                if (true == currentUser.getLogin().isEmpty()) {
%>
                    <sugar:PrintTheeOrders />
<%
                } else {
                    if (false == isCurrentUserAdmin (currentUser)) {
%>
                        <sugar:PrintInboxAndOutbox currentUser="${currentUser.login}"  />
<%
                    }
                }
%>
        </center>
        <div style='display : none; visibility : hidden'>
            Current user login = <input type="text" name="currentUserLogin" id="currentUserLogin" value="${currentUser.login}" />
        </div>
        <div style='display : none; visibility : hidden'>
            <form method='post' action='createOrder.jsp'>
                <input id='receiverLogin' name='receiverLogin' type='text' value='' />
                <input id='senderLogin' name='senderLogin' type='text' value='' />
                <input id='sugar' name='sugar' type='text' value='' />
                <input id='createOrder' type='submit' value='Order' />
            </form>
        </div>