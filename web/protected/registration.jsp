<% String title = "Registration"; %>
<%@include file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript">
            $(document).ready ( function() {
                $(window).resize(function(){
                    placeFooter();
                });
                placeFooter();
                
                function placeFooter() {    
                    var windHeight = $(window).height();
                    var footerHeight = $('#footer').height();
                    var offset = parseInt(windHeight) - parseInt(footerHeight);
                    $('#footer').css('top',offset);
                }
                
                $('#footer').css('display','inline');
                
                $("#errormessage").css("visibility", "hidden").css("width", "500px").css("color", "#AAA");
                $("#loginText").css("border", "1px solid #999");
                $("#passwordText").css("border", "1px solid #999");
                $("#captchaAnswer").css("border", "1px solid #999");

                $("#backButton").on('click', function (e) {
                    window.location.replace("index.jsp");
                });
                $("#backButton").on('mouseover', function (e) {
                    $(this).css ("background-color", "#DDD");
                });
                $("#backButton").on('mouseout', function (e) {
                    $(this).css ("background-color", "white");
                });
        
                $("#clearButton").on('click', function (e) {
                    $("#loginText").css("border", "1px solid #999");
                    $("#passwordText").css("border", "1px solid #999");
                    $("#errormessage").css("visibility", "hidden").css("width", "500px").css("color", "#AAA");
                });
                
                $("#submitButton").on ('click', function (e) {
                    $("#errormessage").css("visibility", "hidden");
                    $("#errormessage").text("");
                    
                    var loginLength = $("#loginText").val().length;
                    var passwordLength = $("#passwordText").val().length;
                    var captchaAnswerLength = $("#captchaAnswer").val().length;
                    
                    var wasError = false;
                    
                    if (captchaAnswerLength === 0) {
                        $("#captchaAnswer").css("border", "1px solid red");
                        $("#errormessage").css("visibility", "visible");
                        $("#errormessage").append("Captcha answer is empty. ");
                        
                        wasError = true;
                    } else {
                        $("#captchaAnswer").css("border", "1px solid #999");
                    }
                    
                    if (loginLength === 0) {
                        $("#loginText").css("border", "1px solid red");
                        $("#errormessage").css("visibility", "visible");
                        $("#errormessage").append("The login is empty. ");
                        
                        wasError = true;
                    } else {
                        $("#loginText").css("border", "1px solid #999")
                    }
                    
                    if (passwordLength === 0) {
                        $("#passwordText").css("border", "1px solid red");
                        $("#errormessage").css("visibility", "visible");
                        $("#errormessage").append("The password is empty. ");
                        
                        wasError = true;
                    } else {
                        $("#passwordText").css("border", "1px solid #999");
                    }
                    
                    return !wasError;
                });
            });
        </script>
    </head>
    <body>
        <div style='position : absolute; float : left'>
            <img id='backButton' style='width : 50px; border : 2px dashed #AAA; padding : 15px' src='../images/back.png'>
        </div>
        <center>
            <span class="big">Enter <font color="#999">new</font> or <font color="#999">existing</font> user credentials</span><br /><br />
            <div id="regform" class='littlegreytext'>
                <form action="../RegistrationServlet" method="post" enctype="multipart/form-data">
                    <b>&nbsp;</b>Login <br />
                    <b>*&nbsp;</b><input id="loginText" type="text" value="" name="login" maxlength="10"><br /><br />
                    <b>&nbsp;</b>Password <br />
                    <b>*&nbsp;</b><input id="passwordText" type="password" value="" name="password" maxlength="10"><br /><br />
                    <b>&nbsp;</b><img src="../simpleCaptcha.png" style="border : 1px solid #999"/><br />
                    <b>*&nbsp;</b><input id="captchaAnswer" type="text" name="answer" /><br /><br />
                    <input type="file" name="file" id="file" accept="image/x-png, image/gif, image/jpeg" /><br /><br />
                    &nbsp;&nbsp;&nbsp;<input id="submitButton" type="submit" value="Login / Register">
                    <input id="clearButton" type="reset" value="Clear">
                    <br />
                </form>
            </div>
            <br />
            <div id="errormessage">
            </div>
        </center>   