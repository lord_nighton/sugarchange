<%@page import="com.sugar.encoders.MDFiveEncoder"%>
<%@page import="com.sugar.users.User"%>
<%@page import="java.util.List"%>
<%@page import="com.sugar.users.UserImpl"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String title = "Change Sugar Here - change usr credentials"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
<script type="text/javascript">
        $(document).ready ( function() {
            $(window).resize(function(){
                    placeFooter();
            });
            placeFooter();
            $('#footer').css('display','inline');

            function placeFooter() {    
                var windHeight = $(window).height();
                var footerHeight = $('#footer').height();
                var offset = parseInt(windHeight) - parseInt(footerHeight);
                $('#footer').css('top',offset);
            }
            
            $("#loginText").css ("border", "1px solid #999");
            $("#passwordText").css ("border", "1px solid #999");
            $("#passwordConfirmText").css ("border", "1px solid #999");
            
            $("#fakeModify").on ('click', function (e) {
                var loginLength = $("#loginText").val().length;
                var passwordLength = $("#passwordText").val().length;
                var passwordConfirmLength = $("#passwordConfirmText").val().length;

                if (0 === loginLength) {
                    $("#loginText").css ("border", "1px solid red");

                    var loginErrorLength = $("#loginError").val().length;
                    if (0 === loginErrorLength) {
                        $("#loginError").text("Login is empty");
                    }
                } else {
                    $("#loginText").css ("border", "1px solid #999");
                    $("#loginError").text("");
                }

                if (0 === passwordLength) {
                    $("#passwordText").css ("border", "1px solid red");

                    var passwordErrorLength = $("#passwordError").val().length;
                    if (0 === passwordErrorLength) {
                        $("#passwordError").text("Password is empty");
                    }
                } else {
                    $("#passwordText").css ("border", "1px solid #999");
                    $("#passwordError").text("");
                }
                
                if (0 === passwordConfirmLength) {
                    $("#passwordConfirmText").css ("border", "1px solid red");

                    var passwordConfirmErrorLength = $("#passwordConfirmError").val().length;
                    if (0 === passwordConfirmErrorLength) {
                        $("#passwordConfirmError").text("Password confirmation is empty");
                    }
                } else {
                    $("#passwordConfirmText").css ("border", "1px solid #999");
                    $("#passwordConfirmError").text("");
                }
                
                if (passwordLength !== passwordConfirmLength) {
                    alert ('The passwords are not equal');
                } else {
                    if ($("#passwordText").val() !== $("#passwordConfirmText").val()) {
                        alert ('The passwods are not equal');
                    } else {
                        $("#modifySubmit").click();
                    }
                }
            });
        });
        </script>
    </head>
    <body>
        <center>
            <br />
            <span class="big">Modify <font color="#999">user</font> credentials</span><br /><br />
            <br />
            <div style='border : 2px dashed #999; width : 850px; padding-top : 10px; padding-bottom : 10px'>
                <form method='post' action='../ModifyUserServlet'>
                    <table style='padding-left : 150px' cellpadding="0" cellspacing="0">
                        <tr>
                            <td style='width : 250px; text-align : right'>Login&nbsp;&nbsp;&nbsp;</td>
                            <td style='width : 150px'>
                                <input id='loginText' disabled name='login' type='text' value='<%= request.getParameter ("modifiedUserLogin") %>' />
                            </td>
                            <td style='width : 450px'>
                                <span id='loginError' style='padding-bottom : 2px; font-size : 12px; color : #B4B4B4'></span>
                            </td>
                        </tr>
                        <tr>
                            <td style='text-align : right'>New password&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <input id='passwordText' name='password' type='password' value='' />
                            </td>
                            <td>
                                <span id='passwordError' style='padding-bottom : 2px; font-size : 12px; color : #B4B4B4'></span>
                            </td>
                        </tr>
                        <tr>
                            <td style='text-align : right'>Confirm password&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <input id='passwordConfirmText' type='password' name='password' value='' />
                            </td>
                            <td>
                                <div id='passwordConfirmError' style='padding-bottom : 2px; font-size : 12px; color : #B4B4B4'></div>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <input id='fakeModify' type='button' value='Modify' />
                    <input type='reset' value='Revert' />
                    <br />
                    <div style='visibility : hidden; display : none'>
                        <input type='text' name='modifiedUserLogin' value='<%= request.getParameter ("modifiedUserLogin") %>' />
                        <input id='modifySubmit' type='submit' value='Modify' />
                    </div>
                </form>
            </div>
        </center>