<%@page import="java.util.ArrayList"%>
<%@page import="com.sugar.utils.UserUtils"%>
<%@page import="com.sugar.encoders.MDFiveEncoder"%>
<%@page import="com.sugar.users.User"%>
<%@page import="java.util.List"%>
<%@page import="com.sugar.users.UserImpl"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String title = "Change Sugar Here - admin panel"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript">
            $(document).ready ( function() {
                $("#loginText").css ("border", "1px solid #999");
                $("#passwordText").css ("border", "1px solid #999");
                
                $(window).resize(function(){
                    placeFooter();
                });
                placeFooter();
                $('#footer').css('display','inline');
                
                function placeFooter() {    
                    var windHeight = $(window).height();
                    var footerHeight = $('#footer').height();
                    var offset = parseInt(windHeight) - parseInt(footerHeight);
                    $('#footer').css('top',offset);
                }
                
                $("#backButton").on('click', function (e) {
                    window.location.replace("index.jsp");
                });
                $("#backButton").on('mouseover', function (e) {
                    $(this).css ("background-color", "#DDD");
                });
                $("#backButton").on('mouseout', function (e) {
                    $(this).css ("background-color", "white");
                });
                
                $("img[id^='delete_']").on( 'click', function (e){
                    var id = $(this).attr ("id");
                    var userLogin = id.substring(7);
                    
                    $('input[type="submit"][value=' + userLogin + ']').click();
                });
                
                $("img[id^='modify_']").on('click', function (e){
                    var id = $(this).attr ("id");
                    var userLogin = id.substring(7);
                    
                    $("#modifiedUserLogin").val (userLogin);
                    $("#modifyUser").click();
                });
                
                $("#createSimpleUser").on('click', function (e){
                    $("#createSimpleUserDiv").css ("visibility", "hidden").css ("display", "none");
                    $("#addUserDiv").css ("visibility", "visible").css ("display", "block");
                });
                
                $("#fakeAddUser").on ('click', function (e){
                    var loginLength = $("#loginText").val().length;
                    var passwordLength = $("#passwordText").val().length;
                    
                    if (0 !== loginLength && 0 !== passwordLength) {
                        $("#addUser").click();
                    }
                    
                    if (0 === loginLength) {
                        $("#loginText").css ("border", "1px solid red");
                        
                        var loginErrorLength = $("#loginError").val().length;
                        if (0 === loginErrorLength) {
                            $("#loginError").text("Login is empty");
                        }
                    } else {
                        $("#loginText").css ("border", "1px solid #999");
                        $("#loginError").text("");
                    }
                    
                    if (0 === passwordLength) {
                        $("#passwordText").css ("border", "1px solid red");
                        
                        var passwordErrorLength = $("#passwordError").val().length;
                        if (0 === passwordErrorLength) {
                            $("#passwordError").text("Password is empty");
                        }
                    } else {
                        $("#passwordText").css ("border", "1px solid #999");
                        $("#passwordError").text("");
                    }
                });
            });
        </script>
    </head>
    <body>
        <div style='position : absolute; float : left'>
            <img id='backButton' style='width : 50px; border : 2px dashed #AAA; padding : 15px' src='../images/back.png'>
        </div>
        <center>
            <%
                boolean isCurrentUserAdmin = UserUtils.isAdmin (currentUser.getLogin (), currentUser.getPassword());
                pageContext.setAttribute ("isCurrentUserAdmin", isCurrentUserAdmin);
            %>
            <c:if test='${null != currentUser && true == isCurrentUserAdmin}'>
                <span class="big">Admin user(s)</span>
                <br />
                <%
                    ArrayList <UserImpl> adminUsers = UserUtils.getAllAdminUsers ();
                    pageContext.setAttribute ("adminUsers", adminUsers);
                %>
                <table class="admintable" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style='width : 50%'><b>Login</b></td><td><b>Password</b></td>
                    </tr>    
                    <%
                        for (UserImpl adminUser : adminUsers) {
                    %>
                        <tr>
                            <td><%= adminUser.getLogin() %></td>
                            <td><%= MDFiveEncoder.transformUsingMDFive (adminUser.getPassword()) %></td>
                        </tr>
                    <%
                        } 
                    %>
                </table>
            </c:if>
<%
            ArrayList <UserImpl> simpleUsers = UserUtils.getAllSimpleUsers ();
            if (null != simpleUsers && false == simpleUsers.isEmpty()) {
%>
            <span class="big">Simple user(s)</span>
                <div style='padding-top : 10px; padding-bottom : 10px' id='createSimpleUserDiv'>
                    <input type='button' value='Create simple user' id='createSimpleUser' />
                </div>
                <div id='addUserDiv' style='visibility : hidden; display : none;'>
                    <form method='post' action='../CreateNewUserServlet'>
                        <table style='padding-left : 150px' cellpadding="0" cellspacing="0">
                            <tr>
                                <td style='text-align : right'>Login&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <input id='loginText' type='text' name='login' value='' />
                                </td>
                                <td style='width : 150px'>
                                    <span id='loginError' style='padding-bottom : 2px; font-size : 12px; color : #B4B4B4'></span>
                                </td>
                            </tr>
                            <tr>
                                <td style='text-align : right'>Password&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <input id='passwordText' type='password' name='password' value='' />
                                </td>
                                <td style='width : 150px'>
                                    <div id='passwordError' style='padding-bottom : 2px; font-size : 12px; color : #B4B4B4'></div>
                                </td>
                            </tr>
                        </table>
                        <input type='button' value='Add user' id='fakeAddUser' />
                        <input type='reset' value='Clear'>
                        
                        <div style='visibility : hidden'>
                            <input type='submit' value='Add user' id='addUser'>    
                        </div>
                    </form>
                </div>
                <form method="post" action="../DeleteUserServlet">
                    <table class='admintable' cellpadding="0" cellspacing="0">
                        <tr style='font-weight : bold'>
                            <td>Login</td>
                            <td>Password</td>
                            <td>Actions</td>
                            <td style='width : 1px'></td>
                        </tr>
<%                          
                            for (UserImpl simpleUser : simpleUsers) {
%>
                                <tr>
                                    <td><%= simpleUser.getLogin() %></td>
                                    <td><%= MDFiveEncoder.transformUsingMDFive (simpleUser.getPassword()) %></td>
                                    <td>
                                        <img style='width : 25px' src="../images/delete.png" id="delete_<%=simpleUser.getLogin()%>">
                                        <img style='width : 25px' src="../images/modify.png" id="modify_<%=simpleUser.getLogin()%>">
                                    </td>
                                    <td>
                                        <input style="visibility : hidden; width : 1px" name="delete" type='submit' value='<%=simpleUser.getLogin()%>' />
                                    </td>
                                </tr>
<%
                            }
%>
                    </table>
                </form>
            <%
                }
            %>    
            <br />
            <div style='border : 1px solid #888; visibility : hidden; display : none'>
                <form method='post' action='modifyUser.jsp'>
                    <input name='modifiedUserLogin' type='text' value='' id='modifiedUserLogin' />
                    <input type='submit' value='Submit' id='modifyUser'/>
                </form>
            </div>
        </center>