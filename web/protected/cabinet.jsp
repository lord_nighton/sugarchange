<%@page import="com.sugar.users.User"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.File"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String title = "Change Sugar Here - my cabinet"; %>
<%@include file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript" src="../fancy/jquery.fancybox.js?v=2.1.4"></script>
	<link rel="stylesheet" type="text/css" href="../fancy/jquery.fancybox.css?v=2.1.4" media="screen" />
        
        <script type="text/javascript">
            $(document).ready (function() {
                $("#fancylink").fancybox ({
                          helpers: {
                                  title : {
                                          type : 'float'
                                  }
                          }
                  });
            });
        </script>
        
        <style type="text/css">
            .fancybox-custom .fancybox-skin {
                    box-shadow: 0 0 50px #222;
            }
        </style>
        
        <script type="text/javascript">
            $(document).ready ( function() {
                $(window).resize(function(){
                    placeFooter();
                });
                placeFooter();
                $('#footer').css('display','inline');
                
                function placeFooter() {    
                    var windHeight = $(window).height();
                    var footerHeight = $('#footer').height();
                    var offset = parseInt(windHeight) - parseInt(footerHeight);
                    $('#footer').css('top',offset);
                }
                
                $("#backButton").on('click', function (e) {
                    window.location.replace("index.jsp");
                });
                $("#backButton").on('mouseover', function (e) {
                    $(this).css ("background-color", "#DDD");
                });
                $("#backButton").on('mouseout', function (e) {
                    $(this).css ("background-color", "white");
                });
                
                $("#submitButton").on('click', function (e) {
                    var filePathLength = $("#file").val().length;
                    if (0 === filePathLength) {
                        return false;
                    }
                });
                
                $("input[id^='delete_']").on ('click', function (e) {
                    var id = $(this).attr ("id");
                    var userAndFileName = id.substring(7);
                    
                    var delimiter = userAndFileName.indexOf ("_");
                    var userLogin = userAndFileName.substring (0, delimiter);
                    var fileName = userAndFileName.substring (delimiter+1);
                    
                    $("#loginToDelete").val (userLogin);
                    $("#fileToDelete").val (fileName);
                    
                    $("#deleteFile").click();
                });
            });
        </script>
    </head>
    <body>
<%!
    private int defineCurrentPageNumber (int minPageNumber, int maxPageNumber, HttpSession session) {
        Integer currentPageNumber = (Integer) session.getAttribute ("currentPageNumber");
        if (null == currentPageNumber) {
            return 0;
        } else {
            if (minPageNumber > currentPageNumber) {
                return minPageNumber;
            } else {
                if (maxPageNumber < currentPageNumber) {
                    return maxPageNumber;
                }
            }
        }
        
        return currentPageNumber;
    }
    
    private String defineUserSugarDir (User user) {
        String sugarsRootDir = getServletContext().getInitParameter ("sugarsDirPath");
        return sugarsRootDir + "/" + user.getLogin();
    }
    
    private File [] defineUserSugars (String userSugarDir) {
        try {
            String root = getServletContext().getRealPath (userSugarDir);
            File sugarsDir = new File (root).getCanonicalFile();
        
            return sugarsDir.listFiles(); 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private int defineSugarsAmount (File [] sugars) {
        if (null == sugars) {
            return 0;
        } else {
            return sugars.length;
        }
    }
    
    private int defineMaxPageNumber (int sugarsAmount) {
        int MAX_SUGARS_PER_PAGE = new Integer ((String) getServletContext().getInitParameter ("maxSugarsPerPage"));
        
        if (0 != sugarsAmount) {
            if (0 == sugarsAmount % MAX_SUGARS_PER_PAGE) {
                return (int) (sugarsAmount / MAX_SUGARS_PER_PAGE) - 1;
            } else {
                return (int) Math.ceil (sugarsAmount / MAX_SUGARS_PER_PAGE);
            }
        }
        return 1;
    }
    
%>
        <div style='position : absolute; float : left'>
            <img id='backButton' style='width : 50px; border : 2px dashed #AAA; padding : 15px' src='../images/back.png'>
        </div>
        <center>
            <span class="big">Upload new sugars</span><br /><br />
            <div id="regform" class='littlegreytext'>
                <form action="../AddSugarServlet" method="post" enctype="multipart/form-data">
                    <input type="file" name="file" id="file" accept="image/x-png, image/gif, image/jpeg" />
                    <br /><br />
                    <input id="submitButton" type="submit" value="Add sugar">
                    <input id="clearButton" type="reset" value="Clear">
                </form>
            </div>
            <div style='padding-top : 50px; '>
                <form method = 'post' action = '../CabinetGaleryServlet'>
                    <table style='width : 1200px; height : 300px;'>
                        <tr>
<%
                            String userSugarDir = defineUserSugarDir (currentUser); 
                            File [] sugars = defineUserSugars (userSugarDir);

                            int sugarsAmount = defineSugarsAmount (sugars);

                            int minPageNumber = 0;
                            int maxPageNumber = defineMaxPageNumber (sugarsAmount);

                            int currentPageNumber = defineCurrentPageNumber (minPageNumber, maxPageNumber, session);
%>

                        <c:set var='sugarsAmount' scope="page" value="<%= sugarsAmount%>" />
                        <c:set var='currentPageNumber' scope="session" value="<%= currentPageNumber%>" />

                        <c:set var='minPageNumber' scope="page" value="<%= minPageNumber%>" />                            
                        <c:set var='maxPageNumber' scope="page" value="<%= maxPageNumber%>" />

                        <c:choose>
                            <c:when test="${0 != sugarsAmount && currentPageNumber != minPageNumber}">
                                <td style='width : 50px'>
                                    <input type='submit' value='<' name='back' />
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td style='width : 50px'>&nbsp;</td>
                            </c:otherwise>
                        </c:choose>
                                
                            <td style='padding-left : 23px; width : 1100px'>
                                <c:if test="${0 != sugarsAmount}" >
<%
                                    int MAX_SUGARS_PER_PAGE = new Integer ((String) getServletContext().getInitParameter ("maxSugarsPerPage"));
                                    File [] neededPage = Arrays.copyOfRange (sugars, MAX_SUGARS_PER_PAGE*(currentPageNumber), MAX_SUGARS_PER_PAGE*(currentPageNumber) + MAX_SUGARS_PER_PAGE);
%>
                                    <c:forEach begin="0" var="sugar" items="<%= neededPage%>">
                                        <c:if test="${null != sugar}">
                                            <div style='float : left; text-align :center'>
                                                <a id="fancylink" href="../<%= userSugarDir%>/${sugar.getName()}">
                                                    <img style='border : 1px solid #999; width : 200px; height : 200px;' src='../<%= userSugarDir%>/${sugar.getName()}' />
                                                </a>
                                                <br />
                                                <input type='button' value='X' id='delete_${currentUser.login}_${sugar.getName()}' />
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </td>
                            <c:choose>
                                <c:when test="${0 != sugarsAmount && currentPageNumber != maxPageNumber}">
                                    <td style='width : 40px'>
                                        <input type='submit' value='>' name='forward' />
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td style='width : 40px'>&nbsp;</td>
                                </c:otherwise>                                    
                            </c:choose>
                        </tr>
                    </table>
                    <br /><br />
                    <c:if test="${sugarsAmount > 5}">
                        <c:forEach var="i" begin="0" end="${maxPageNumber}">
                            <c:choose>
                                <c:when test="${i == currentPageNumber}">
                                    <input style='font-weight : bold; font-size : 25px;' type='submit' value='${i+1}' name='page${i}' />    
                                </c:when>
                                <c:otherwise>
                                    <input type='submit' value='${i+1}' name='page${i}' />    
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </c:if>
                </form>
            </div>
            <div style='visibility : hidden; display : none'>
                <form method='post' action='../DeleteSugarServlet'>
                    <input id='loginToDelete' type='text' name='loginToDelete' value='' />
                    <input id='fileToDelete' type='text' name='fileToDelete' value='' />
                    <input id='deleteFile' type='submit' value='Delete file' />
                </form>
            </div>
        </center>  