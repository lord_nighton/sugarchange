package com.sugar.filters;

import com.sugar.enums.Errors;
import com.sugar.users.User;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

public class CabinetFilter implements Filter {
    
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        
        final User user = (User) req.getSession().getAttribute ("currentUser");
        
        if (true == user.isUserUnloginned ()) {
            throw new IllegalStateException (Errors.ONLY_LOGINNED_USER_AREA.toString());
        } else {
            chain.doFilter (request, response);
        }
    }

    @Override
    public void destroy() {        
    }

    @Override
    public void init (FilterConfig filterConfig) throws ServletException {
    }
    
}