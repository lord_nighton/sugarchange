package com.sugar.filters;

import java.io.IOException;
import javax.servlet.Filter;
import com.sugar.enums.Errors;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author LordNighton
 * 
 */

@WebFilter(filterName = "BrowserFilter", urlPatterns = {"/protected/*"})
public class BrowserFilter implements Filter {
    
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        
        if (true == req.getHeader ("User-Agent").contains ("MSIE")) {
            throw new IllegalStateException (Errors.INCORRECT_BROWSER.toString());
        } else {
            chain.doFilter (request, response);
        }
    }

    @Override
    public void destroy() {        
    }

    @Override
    public void init (FilterConfig filterConfig) throws ServletException {
    }

}