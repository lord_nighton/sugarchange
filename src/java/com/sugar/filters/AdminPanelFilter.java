package com.sugar.filters;

import java.io.IOException;
import com.sugar.users.User;
import javax.servlet.Filter;
import com.sugar.enums.Errors;
import com.sugar.utils.UserUtils;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author LordNighton
 * 
 */

public class AdminPanelFilter implements Filter {
    
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final User user = (User) req.getSession().getAttribute ("currentUser");
        
        if (true == user.isUserUnloginned ()) {
            throw new IllegalStateException (Errors.ACCESS_IS_DENIED.toString());
        } else {
            if (true == UserUtils.isAdmin (user.getLogin(), user.getPassword ())) {
                chain.doFilter (request, response);
            } else {
                throw new IllegalStateException (Errors.RESTRICTED_AREA.toString());
            }
        }
    }

    @Override
    public void destroy() {        
    }

    @Override
    public void init (FilterConfig filterConfig) throws ServletException {
    }
    
}