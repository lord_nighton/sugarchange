package com.sugar.enums;

/**
 *
 * @author LordNighton
 * 
 */

public enum Errors {

    INCORRECT_BROWSER ("You are using an incorrect version of browser. IE is prohibited."),
    
    ACCESS_IS_DENIED ("An access is denied. Login to the system first."),
    
    DIRECT_ACCESS ("There was no error. Probably, you have visited this page directly."),
    
    RESTRICTED_AREA ("This user is not allowed to visit this restricted area."),
    
    ONLY_LOGINNED_USER_AREA ("Only loginned users are allowed to visit their cabinets.");
    
    private Errors (String text) {
        this.text = text;
    }
    
    @Override
    public String toString () {
        return this.text;
    }
   
    private String text;
    
}