package com.sugar.users;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 
 * @author LordNighton
 * 
 */

public class UserImpl implements User, Serializable {

    private void writeObject (ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject (); 
    }
    
    private void readObject (ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject ();  
    }
    
    private String login = "";
    
    private String password = "";
    
    private String mail = "";
    
    private Address address = new Address ();
    
    @Override
    public String getLogin() {
        return this.login;
    }
    
    @Override
    public void setLogin (String login) {
        this.login = login;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword (String password) {
        this.password = password;
    }
    
    @Override
    public boolean isUserUnloginned () {
        return true == getLogin().isEmpty() || true == getPassword().isEmpty();
    }

    @Override
    public String getMail() {
        return mail;
    }

    @Override
    public void setMail (String mail) {
        this.mail = mail;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public void setAddress (Address address) {
        this.address = address;
    }
    
}