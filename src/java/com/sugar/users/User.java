package com.sugar.users;

/*
 *
 * @author LordNighton
 * 
 */

public interface User {

    public String getLogin ();
    
    public void setLogin (String login);
    
    public String getPassword ();
    
    public void setPassword (String password); 
   
    public boolean isUserUnloginned ();
    
    public String getMail ();

    public void setMail (String mail);
    
    public Address getAddress ();

    public void setAddress (Address address);
    
}