package com.sugar.utils;

import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import javax.sql.DataSource;
import com.sugar.users.User;
import javax.naming.Context;
import com.sugar.users.Address;
import com.sugar.users.UserImpl;
import java.sql.PreparedStatement;
import javax.naming.InitialContext;

/**
 *
 * @author LordNighton
 *
 */

public class UserUtils {

    public static final String GET_ADMIN_USER_BY_LOGIN_AND_PASSWORD = "SELECT * from sugarsdb.admin_user where login = '%s' and password = '%s'";
    
    public static final String GET_SIMPLE_USER_BY_LOGIN = "SELECT * from sugarsdb.simple_user where login = '%s'";
    
    public static final String INSERT_USER = "INSERT INTO sugarsdb.simple_user (login, password) values ('%s', '%s')";
    
    public static final String GET_ALL_SIMPLE_USERS = "SELECT * from sugarsdb.simple_user order by login ASC";
    
    public static final String GET_ALL_ADMIN_USERS = "SELECT * from sugarsdb.admin_user order by login ASC";
    
    public static final String UPDATE_USER_PASSWORD = "UPDATE sugarsdb.simple_user SET password = '%s' where login = '%s'";
    
    public static final String DELETE_USER_BY_LOGIN = "DELETE FROM sugarsdb.simple_user where login = '%s'";
    
    public static final String UPDATE_USER_ADDRESS = "UPDATE sugarsdb.simple_user SET mail = '%s', city = '%s', country = '%s', flatNumber = '%s', homeNumber = '%s', indexCode = '%s', street = '%s' where login = '%s'";
    
    private static Connection getConnection () {
        try {
            Context ctx = new InitialContext();
            DataSource dataSource = (DataSource) ctx.lookup ("java:comp/env/jdbc/sugarsdb");
            
            return dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private static void closeConnection (Connection connection) {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static boolean isAdmin (String login, String password) {
        Connection connection = getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement (String.format (GET_ADMIN_USER_BY_LOGIN_AND_PASSWORD, login, password)); 
            ResultSet resultSet = statement.executeQuery ();
            if (true == resultSet.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection (connection);
        }
        return false;
    }
    
    public static User getUserByLogin (String login) {
        Connection connection = getConnection();
        User user = null;
        try {
            PreparedStatement statement = connection.prepareStatement (String.format (GET_SIMPLE_USER_BY_LOGIN, login));
            ResultSet resultSet = statement.executeQuery();
            if (true == resultSet.next()) {
                user = new UserImpl();
                user.setLogin (resultSet.getString ("login"));
                user.setPassword (resultSet.getString ("password"));
                user.setMail (resultSet.getString ("mail"));
                
                Address address = new Address ();
                address.setCity (resultSet.getString ("city"));
                address.setCountry (resultSet.getString ("country"));
                address.setFlatNumber (resultSet.getString ("flatNumber"));
                address.setHomeNumber (resultSet.getString ("homeNumber"));
                address.setIndexCode (resultSet.getString ("indexCode"));
                address.setStreet (resultSet.getString ("street"));
                
                user.setAddress (address);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection (connection);
        }
        return user;
    }
    
    public static void addUserToDB (String login, String password) {
        Connection connection = getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement (String.format (INSERT_USER, login, password));
            statement.executeUpdate ();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection (connection);
        }
    }
    
    public static ArrayList <UserImpl> getAllSimpleUsers () {
        return getUsersByQuery (GET_ALL_SIMPLE_USERS);
    }
    
    public static ArrayList <UserImpl> getAllAdminUsers () {
        return getUsersByQuery (GET_ALL_ADMIN_USERS);
    }
    
    private static ArrayList <UserImpl> getUsersByQuery (String query) {
        Connection connection = getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement (query);
            ResultSet resultSet = statement.executeQuery ();
            
            ArrayList <UserImpl> allUsers = new ArrayList <UserImpl> ();
            while (true == resultSet.next ()) {
                UserImpl user = new UserImpl();
                user.setLogin (resultSet.getString ("login"));
                user.setPassword (resultSet.getString ("password"));
                
                allUsers.add (user);
            }
            return allUsers;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection (connection);
        }
        return null;
    }
    
    public static void changeUserPassword (String login, String newPassword) {
        Connection connection = getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement (String.format (UPDATE_USER_PASSWORD, newPassword, login));
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection (connection);
        }
    }
    
    public static void deleteUserByLogin (String login) {
        Connection connection = getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement (String.format (DELETE_USER_BY_LOGIN, login));
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection (connection);
        }
    }
    
    public static void updateUserAddress (UserImpl user) {
        Connection connection = getConnection();
        
        String mail = user.getMail();
        String city = user.getAddress().getCity();
        String country = user.getAddress().getCountry();
        String flatNumber = user.getAddress().getFlatNumber();
        String homeNumber = user.getAddress().getHomeNumber();
        String indexCode = user.getAddress().getIndexCode();
        String street = user.getAddress().getStreet();
        String login = user.getLogin();
        
        String addressQuery = String.format (UPDATE_USER_ADDRESS, mail, city, country, flatNumber, homeNumber, indexCode, street, login);
        try {
            PreparedStatement statement = connection.prepareStatement (addressQuery);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection (connection);
        }
    }
    
}