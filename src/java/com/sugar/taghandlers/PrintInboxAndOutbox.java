package com.sugar.taghandlers;

import java.io.File;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;
import javax.servlet.ServletContext;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.JspException;
import org.apache.commons.io.FileUtils;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 *
 * @author LordNighton
 * 
 */

public class PrintInboxAndOutbox extends SimpleTagSupport {

    private String currentUser;
    
    @Override
    public void doTag () throws JspException {
        PageContext pageContext = (PageContext) getJspContext();
        
        ServletContext servletContext = pageContext.getRequest().getServletContext();
        
        JspWriter out = pageContext.getOut();
        try {
            ArrayList <File> allOrders = defineAllOrders (servletContext);
            
            ArrayList <File> iSend = defineOrdersISend (allOrders);
            ArrayList <File> theySend = defineOrdersTheySend (allOrders);        
            
            out.println ("<div style='padding-bottom : 100px; padding-top : 15px; width : 988px; padding-right : 13px'>");
            out.println ("<table cellpadding='0' cellspacing='0' style='width : 990px'>");
            out.println ("<tr><td style='padding-bottom : 20px; width : 545px; text-align  : center'>"
                    + "<div class='littlegreytext'>"
                    + "They should send"
                    + "</div></td><td style='width : 545px; text-align  : center'>"
                    + "<div class='littlegreytext'>"
                    + "I should send"
                    + "</div></td></tr></table>");
            
            out.println ("<table cellpadding='0' cellspacing='0' style='width : 990px; border : 2px dashed #999'>");
            out.println ("<tr>");
            out.println ("<td style='width : 50%; vertical-align : top; border-right : 2px dashed #999; text-align : center; background-color : #C3D9FF'>");
            
            for (File order : theySend) {
                out.println (defineSugarPath (order));
                out.println ("<br />");
            }
            
            out.println ("</td>");
            out.println ("<td style='width : 50%; vertical-align : top; text-align : center; background-color : #f5cae5'>");
            
            int counter = 0;
            for (File order : iSend) {
                out.println ("<br />");
                out.println (defineSugarPath (order, counter));
                out.println ("<br />");
                out.println ("<input id='sender" + counter +"' type='submit' value='Send' />");
                out.println ("<br />");
                counter++;
            }
            out.println ("</td></tr></table></div>");
        } catch (java.io.IOException ex) {
            throw new JspException ("Error in PrintTheeOrders tag", ex);
        }
    }
    
    private ArrayList<File> defineAllOrders (ServletContext servletContext) throws IOException {
        String ordersDirPath = servletContext.getInitParameter ("orders.folder");
        String root = servletContext.getRealPath (ordersDirPath);
        File ordersRootDir = new File (root).getCanonicalFile();
            
        return new ArrayList <File> ((List <File>) FileUtils.listFiles (ordersRootDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE));
    }

    private ArrayList <File> defineOrdersISend (ArrayList <File> allOrders) {
        ArrayList <File> iSendOrders = new ArrayList <File> ();
        
        for (File order : allOrders) {
            String fileName = order.getName();

            String [] tokens = fileName.split ("_");

            String sender = tokens [1];
            if (true == sender.equals (currentUser)) {
                iSendOrders.add (order);
            }
        }
        
        return iSendOrders;
    }

    private ArrayList<File> defineOrdersTheySend (ArrayList <File> allOrders) {
        ArrayList <File> theySendOrders = new ArrayList <File> ();
        
        for (File order : allOrders) {
            String fileName = order.getName();

            String [] tokens = fileName.split ("_");

            String receiver = tokens [0];
            if (true == receiver.equals (currentUser)) {
                theySendOrders.add (order);
            }
        }
        
        return theySendOrders;
    }
    
    private String defineSugarPath (File order) throws IOException {
        String fileName = order.getName();

        String [] tokens = fileName.split ("_");

        String [] fileTokens = tokens[2].split("#");
        String sugarFileName = fileTokens[0] + "." + fileTokens[1];

        String sugarPath = "../sugars/" + tokens[1] + "/" + sugarFileName.substring (0, sugarFileName.length() - 4);

        return "<img style='border : 1px dashed #999; width : 150px; height : 150px;' src='" + sugarPath + "' />";
    }
    
    private String defineSugarPath (File order, int counter) throws IOException {
        String fileName = order.getName();

        String [] tokens = fileName.split ("_");

        String [] fileTokens = tokens[2].split("#");
        String sugarFileName = fileTokens[0] + "." + fileTokens[1];

        String sugarPath = "../sugars/" + tokens[1] + "/" + sugarFileName.substring (0, sugarFileName.length() - 4);

        return "<img id='sugar"+ counter + "' style='border : 1px dashed #999; width : 150px; height : 150px;' src='" + sugarPath + "' />";
    }

    public String getCurrentUser () {
        return currentUser;
    }

    public void setCurrentUser (String currentUser) {
        this.currentUser = currentUser;
    }

}