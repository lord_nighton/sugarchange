package com.sugar.taghandlers;

import java.io.File;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;
import javax.servlet.ServletContext;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.JspException;
import org.apache.commons.io.FileUtils;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 *
 * @author LordNighton
 * 
 */

public class PrintTheeOrders extends SimpleTagSupport {

    @Override
    public void doTag () throws JspException {
        PageContext pageContext = (PageContext) getJspContext();  
        
        ServletContext servletContext = pageContext.getRequest().getServletContext();
        
        JspWriter out = pageContext.getOut();
        try {
            ArrayList <File> allOrders = defineAllOrders (servletContext);
            if (0 == allOrders.size()) {
                out.println ("<div style='padding-bottom : 100px; padding-top : 15px; width : 988px; padding-right : 13px'>");
                out.println ("<div class='littlegreytext'>Orders will be displayed after their appearance</div><br />");
            } else {
                printThreeOrders (out, allOrders);
            }
        } catch (java.io.IOException ex) {
            throw new JspException ("Error in PrintTheeOrders tag", ex);
        }
    }
    
    private ArrayList<File> defineAllOrders (ServletContext servletContext) throws IOException {
        String ordersDirPath = servletContext.getInitParameter ("orders.folder");
        String root = servletContext.getRealPath (ordersDirPath);
        File ordersRootDir = new File (root).getCanonicalFile();
            
        return new ArrayList <File> ((List <File>) FileUtils.listFiles (ordersRootDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE));
    }

    private void printThreeOrders (JspWriter out, ArrayList <File> allOrders) throws IOException {
        out.println ("<div style='padding-bottom : 100px; padding-top : 15px; width : 988px; padding-right : 13px'>");
        out.println ("<div class='littlegreytext'>Some Random Orders</div><br />");
        out.println ("<table style='border : 2px dashed #999; background-color : #98e4c8; width : 988px; text-align : center'><tr><td style='text-align : center; width : 300px'>Sender</td><td style='text-align : center; width : 300px'>Sugar</td><td style='text-align : center; width : 300px'>Receiver</td></tr>");
        if (allOrders.size () > 3) {
            for (int i = 0; i < 3; i++) {
                printOrder (out, allOrders.get (i));
            }
        } else {
            for (File order : allOrders) {
                printOrder (out, order);
            }
        }
        out.println ("</table>");
        out.println ("</div>");
    }

    private void printOrder (JspWriter out, File order) throws IOException {
        String fileName = order.getName();

        String [] tokens = fileName.split ("_");

        String receiver = tokens [0];
        String sender = tokens [1];

        String [] fileTokens = tokens[2].split("#");
        String sugarFileName = fileTokens[0] + "." + fileTokens[1];

        String sugarPath = "../sugars/" + tokens[1] + "/" + sugarFileName.substring (0, sugarFileName.length() - 4);

        out.println ("<tr><td>" + sender + "</td><td><img style='width : 150px; height : 150px;' src='" + sugarPath + "' /></td><td>" + receiver + "</td></tr>");
    }
    
}