package com.sugar.galery;

import java.io.File;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 *
 * @author LordNighton
 *
 */

public class RandomGaleryImagesFetcher {

    public static String defineRandomImagePathForLoginnedUser (ServletContext context, String userLogin) {
        ArrayList <String> anotherUsersSugars = defineSugarPathsOfAnotherUsers (context, userLogin);
                
        return defineRandomSugar (anotherUsersSugars);
    }
    
    private static ArrayList<String> defineSugarPathsOfAnotherUsers (ServletContext context, String userLogin) {
        String sugarsDirPath = context.getInitParameter ("sugarsDirPath");
        String root = context.getRealPath (sugarsDirPath);
        
        ArrayList <String> sugarsFilePaths = new ArrayList <String> ();
        try {
            File sugarsRootDir = new File (root).getCanonicalFile();
            List <File> files = (List<File>) FileUtils.listFiles (sugarsRootDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
            for (File next : files) {
                String filePath = next.getCanonicalPath();
                String [] tokens = filePath.split ("\\\\");
                String sugarUserLogin = tokens [tokens.length - 2]; 
                
                if (false == sugarUserLogin.equals (userLogin)) {
                    sugarsFilePaths.add ("../sugars/" + sugarUserLogin + "/" + next.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sugarsFilePaths;
    }
    
    public static String defineRandomImagePathForVisitor (ServletContext context) {
        ArrayList <String> allSugars = defineAllSugarPaths (context);
        
        return defineRandomSugar (allSugars);
    }
    
    private static ArrayList <String> defineAllSugarPaths (ServletContext context) {
        String sugarsDirPath = context.getInitParameter ("sugarsDirPath");
        String root = context.getRealPath (sugarsDirPath);
        
        ArrayList <String> sugarsFilePaths = new ArrayList <String> ();
        try {
            File sugarsRootDir = new File (root).getCanonicalFile();
            List <File> files = (List<File>) FileUtils.listFiles (sugarsRootDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
            for (File next : files) {
                String filePath = next.getCanonicalPath();
                String [] tokens = filePath.split ("\\\\");
                String userLogin = tokens [tokens.length - 2]; 
                
                sugarsFilePaths.add ("../sugars/" + userLogin + "/" + next.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sugarsFilePaths;
    }

    private static String defineRandomSugar (ArrayList<String> sugars) {
        if (null == sugars || true == sugars.isEmpty()) {
            return "../images/nosugars.png";
        } else {
            Random random = new Random();
            int randomNumber = random.nextInt (sugars.size());
        
            return sugars.get (randomNumber);
        }
    }

}