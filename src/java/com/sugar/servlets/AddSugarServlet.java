package com.sugar.servlets;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.io.IOException;
import com.sugar.users.User;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author LordNighton
 * 
 */

public class AddSugarServlet extends HttpServlet {

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        boolean isMultipart = ServletFileUpload.isMultipartContent (request);
        if (true == isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload (factory);

            FileItem sugarFileItem = null;            
            try {
                List items = upload.parseRequest (request);
                Iterator iterator = items.iterator();
                while (true == iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();

                    if (false == item.isFormField()) {
                        sugarFileItem = item;
                    }
                }
                
                User user = (User) request.getSession().getAttribute ("currentUser");

                String [] tokens = sugarFileItem.getName().split("\\.");
                if (true == isImage (tokens [1])){
                    uploadSugar (sugarFileItem, user.getLogin());
                }
                
                response.sendRedirect ("protected/cabinet.jsp");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            response.sendRedirect ("protected/index.jsp");
        }
    }
    
    private void uploadSugar (FileItem sugarFile, String login) {
        try {
            String sugarsDirPath = getServletContext().getInitParameter ("sugarsDirPath") + "/" + login + "/";
            String root = getServletContext().getRealPath (sugarsDirPath);

            File sugarsRootDir = new File (root).getCanonicalFile();
            if (false == sugarsRootDir.exists()) {
                sugarsRootDir.mkdirs();
            }

            String [] tokens = sugarFile.getName().split("\\.");
            
            File uploadedFile = new File (sugarsRootDir + "/" + new Date().getTime() + "." + tokens[1]);
            sugarFile.write (uploadedFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private boolean isImage (String extension) {
        boolean isPNG = "png".equalsIgnoreCase (extension);
        boolean isJPG = "jpg".equalsIgnoreCase (extension);
        boolean isJPEG = "jpeg".equalsIgnoreCase (extension);
        boolean isGIF = "gif".equalsIgnoreCase (extension);
        
        return isPNG || isJPG || isGIF || isJPEG;
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo () {
        return "Add Sugar Servlet";
    }
    
}