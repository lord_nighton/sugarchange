package com.sugar.servlets;

import java.io.File;
import java.util.List;
import java.util.Iterator;
import java.io.IOException;
import com.sugar.users.User;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author LordNighton
 *
 */

@WebServlet(name="ChangeAvaServlet", urlPatterns={"/ChangeAvaServlet"})
public class ChangeAvaServlet extends HttpServlet {
   
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        boolean isMultipart = ServletFileUpload.isMultipartContent (request);
        if (true == isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload (factory);

            FileItem avaFileItem = null; 
            try {
                List items = upload.parseRequest (request);
                Iterator iterator = items.iterator();
                while (true == iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();

                    if (false == item.isFormField()) {
                        avaFileItem = item;
                    }
                }
                
                User user = (User) request.getSession().getAttribute ("currentUser");
                
                String [] tokens = avaFileItem.getName().split("\\.");
                if (true == isImage (tokens [1])){
                    changeAva (avaFileItem, user.getLogin());
                }
                
                response.sendRedirect ("protected/index.jsp");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private boolean isImage (String extension) {
        boolean isPNG = "png".equalsIgnoreCase (extension);
        boolean isJPG = "jpg".equalsIgnoreCase (extension);
        boolean isJPEG = "jpeg".equalsIgnoreCase (extension);
        boolean isGIF = "gif".equalsIgnoreCase (extension);
        
        return isPNG || isJPG || isGIF || isJPEG;
    }
    
    private void changeAva (FileItem avaFile, String login) {
        try {
            String [] tokens = avaFile.getName().split("\\.");
            String fileName = login + "." + tokens [1];
            
            String avatarsDirPath = getServletContext().getInitParameter ("avatarsDirPath");
            String root = getServletContext().getRealPath (avatarsDirPath);

            File avatarsDir = new File (root).getCanonicalFile();
            if (false == avatarsDir.exists()) {
                avatarsDir.mkdirs();
            }

            File uploadedFile = new File (avatarsDir + "/" + fileName);
            avaFile.write (uploadedFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public String getServletInfo () {
        return "Short description";
    }

}