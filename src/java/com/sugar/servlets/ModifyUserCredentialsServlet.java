package com.sugar.servlets;

import java.io.IOException;
import com.sugar.users.UserImpl;
import com.sugar.utils.UserUtils;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 *
 */

public class ModifyUserCredentialsServlet extends HttpServlet {
   
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        String userLogin = (String) request.getParameter ("userLogin");
        
        UserImpl user = (UserImpl) UserUtils.getUserByLogin (userLogin);
        if (null != user) {
            user.setMail (request.getParameter ("mail"));
            user.getAddress().setCity (request.getParameter ("city"));
            user.getAddress().setCountry (request.getParameter ("country"));
            user.getAddress().setFlatNumber (request.getParameter ("flatNumber"));
            user.getAddress().setHomeNumber (request.getParameter ("homeNumber"));
            user.getAddress().setIndexCode (request.getParameter ("indexCode"));
            user.getAddress().setStreet (request.getParameter ("street"));
            
            updateDBUserAddress (user);
            
            request.getSession().setAttribute ("currentUser", user);
        }
        response.sendRedirect ("protected/index.jsp");
    }

    private void updateDBUserAddress (UserImpl user) {
        UserUtils.updateUserAddress (user);
    }
    
    @Override
    public String getServletInfo () {
        return "Short description";
    }

}