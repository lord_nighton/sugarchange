package com.sugar.servlets;

import java.io.IOException;
import com.sugar.users.User;
import com.sugar.utils.UserUtils;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        final User currentUser = (User) request.getSession().getAttribute ("currentUser");
        
        final String login = (String) request.getParameter ("login");
        final String password = (String) request.getParameter ("password");
        
        if (true == UserUtils.isAdmin (login, password)) {
            currentUser.setLogin (login);
            currentUser.setPassword (password);

            response.sendRedirect ("protected/index.jsp");
        } else {
            User dbUser = UserUtils.getUserByLogin (login);
            if (null == dbUser) {
                response.sendRedirect ("protected/registration.jsp");
            } else {
                if (true == dbUser.getLogin().equals (login) && dbUser.getPassword ().equals (password)) {
                    currentUser.setLogin (dbUser.getLogin ());
                    currentUser.setPassword (dbUser.getPassword ());
                    currentUser.setMail (dbUser.getMail());
                    currentUser.setAddress (dbUser.getAddress());
                    
                    response.sendRedirect ("protected/index.jsp");
                } else {
                    response.sendRedirect ("protected/registration.jsp");
                }
            }
        }
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo () {
        return "Login Servlet";
    }
    
}