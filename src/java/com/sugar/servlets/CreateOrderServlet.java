package com.sugar.servlets;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 *
 */

@WebServlet(name="CreateOrderServlet", urlPatterns={"/CreateOrderServlet"})
public class CreateOrderServlet extends HttpServlet {
   
    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    } 

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");

        String senderLogin = request.getParameter ("senderLogin");
        String receiverLogin = request.getParameter ("receiverLogin");
        String sugar = request.getParameter ("sugar");

        ServletContext context = getServletContext();
        String ordersFolderPath = context.getInitParameter ("orders.folder");

        String realPath = context.getRealPath (ordersFolderPath);
        File orderFolder = new File (realPath).getCanonicalFile();
        String orderFileName = constructOrderFileName (receiverLogin, senderLogin, sugar);

        if (true == orderFolder.exists()) {
            File order = new File (realPath + "/" + orderFileName);
            order.createNewFile();
            
            response.sendRedirect ("protected/index.jsp");
        } 
    }
    
    private String constructOrderFileName (String receiverLogin, String senderLogin, String sugar) {
        String replaced = sugar.replaceAll ("\\.", "#");
        
        StringBuilder orderFileName = new StringBuilder();
        orderFileName.append (receiverLogin).append ("_").append (senderLogin).append ("_").append (replaced).append (".txt");
        
        return orderFileName.toString ();
    }

    @Override
    public String getServletInfo () {
        return "Short description";
    }

}