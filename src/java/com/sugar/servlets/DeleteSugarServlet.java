package com.sugar.servlets;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

public class DeleteSugarServlet extends HttpServlet {

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }
    
    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String login = request.getParameter ("loginToDelete");
            String fileName = request.getParameter ("fileToDelete");
            
            String sugarsDirPath = getServletContext().getInitParameter ("sugarsDirPath") + "/" + login + "/";
            String root = getServletContext().getRealPath (sugarsDirPath);

            File sugarsRootDir = new File (root).getCanonicalFile();
            if (false == sugarsRootDir.exists()) {
                sugarsRootDir.mkdirs();
            }

            File [] sugars = sugarsRootDir.listFiles(); 
            
            for (File sugar : sugars) {
                if (true == sugar.getName().equals (fileName)) {
                    if (true == sugar.delete()) {
    			System.out.println (sugar.getName() + " is deleted!");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } 
        
        response.sendRedirect ("protected/cabinet.jsp");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}