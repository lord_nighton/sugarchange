package com.sugar.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

public class CabinetGaleryServlet extends HttpServlet {

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        Integer currentPageNumber = (Integer) request.getSession().getAttribute ("currentPageNumber");

        if (null == currentPageNumber) {
            currentPageNumber = 0;
        }
        
        if (true == isBackPressed (request)) {
            currentPageNumber--;
        }
        
        if (true == isForwardPressed (request)) {
            currentPageNumber++;
        }
        
        String pressedPage = definePressedPageButton (request);
        if (null != pressedPage) {
            Integer integerPressedPage = new Integer (pressedPage);
            currentPageNumber = integerPressedPage;
        }
        
        request.getSession().setAttribute ("currentPageNumber", currentPageNumber);
        
        response.sendRedirect ("protected/cabinet.jsp");
    }
    
    private boolean isBackPressed (HttpServletRequest request) {
        String backPressed = (String) request.getParameter ("back");
        if (null == backPressed) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isForwardPressed (HttpServletRequest request) {
        String forwardPressed = (String) request.getParameter ("forward");
        if (null == forwardPressed) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo () {
        return "Cabinet Galery Servlet"; 
    }

    private String definePressedPageButton (HttpServletRequest request) {
        Pattern pattern = Pattern.compile ("page([0-9]+)");
        Matcher matcher = null;
        
        Enumeration <String> enumParams = request.getParameterNames();
        
        ArrayList <String> listParams = Collections.list (enumParams);
        for (String nextParam : listParams) {
            matcher = pattern.matcher (nextParam);
            if (true == matcher.matches ()) {
                return matcher.group (1);
            }
        }

        return null;
    }

}