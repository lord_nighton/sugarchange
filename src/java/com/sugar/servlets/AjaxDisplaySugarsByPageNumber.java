package com.sugar.servlets;

import com.sugar.users.UserImpl;
import java.io.File;
import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import org.apache.commons.io.FileUtils;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 *
 * @author LordNighton
 *
 */

@WebServlet(name="AjaxDisplayAllSugarsServlet_1", urlPatterns={"/AjaxDisplayAllSugarsServlet_1"})
public class AjaxDisplaySugarsByPageNumber extends HttpServlet {
   
    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    } 

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            ArrayList <File> sugars = defineAllSugars (request);
            if (0 == sugars.size()) {
                out.println ("<center><span style='font-size : 20px'>Sorry, other users haven't added the sugars yet</span><br /><br />Please, add the sugars using <a style='text-decoration : none; font-weight : bold; color : black' href='cabinet.jsp'>cabinet</a><br /><br /></center>");
            } else {
                int pagesAmount = definePagesAmount (sugars, request);
            
                List <File> sugarsToShow = defineRange (pagesAmount, sugars, request);

                printSugars (sugarsToShow, out);
                printPager (pagesAmount, out, request);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally { 
            out.close();
        }
    }
    
    private ArrayList <File> defineAllSugars (HttpServletRequest request) throws IOException {
        UserImpl user = (UserImpl) request.getSession().getAttribute ("currentUser");
        String login = user.getLogin ();        
        
        String sugarsDirPath = getServletContext().getInitParameter ("sugarsDirPath");
        String root = getServletContext().getRealPath (sugarsDirPath);
        File sugarsRootDir = new File (root).getCanonicalFile();
            
        ArrayList <File> allSugars = new ArrayList <File> ((List <File>) FileUtils.listFiles (sugarsRootDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE));
        ArrayList <File> notCurrentUserSugars = new ArrayList <File> ();
        for (File sugar : allSugars) {
            if (false == sugar.getPath().contains (new StringBuilder().append ("\\").append (login).append ("\\").toString())) {
                notCurrentUserSugars.add (sugar);
            }
        }
        return notCurrentUserSugars;
    }
    
    private int definePagesAmount (List <File> sugars, HttpServletRequest request) {
        int sugarsPerPage = new Integer (request.getParameter ("pageContains"));
        
        int sugarsAmount = sugars.size();
        
        if (sugarsAmount < sugarsPerPage) {
            return 1;
        } else {
            if (0 == sugarsAmount % sugarsPerPage) {
                return (int) (sugarsAmount / sugarsPerPage);
            } else {
                return (int) Math.ceil (sugarsAmount / sugarsPerPage) + 1;
            }
        }
    }
    
    private List<File> defineRange (int pagesAmount, ArrayList <File> sugars, HttpServletRequest request) {
        int pageNumber = new Integer (request.getParameter ("pageNumber"));
        int pageContains = new Integer (request.getParameter ("pageContains"));
        
        if (pageContains > sugars.size()) {
            return sugars;
        } else {
            if (pagesAmount - 1 == pageNumber) {
                int leftBound = pageContains * pageNumber;
                return (List <File>) sugars.subList (leftBound, sugars.size());
            } else {
                int leftBound = pageContains * pageNumber;
                int rightBound = pageContains * pageNumber + pageContains;
                return (List <File>) sugars.subList (leftBound, rightBound);
            }
        }
    }
    
    private void printSugars (List <File> neededPage, PrintWriter out) throws IOException {
        int counter = 0;
        for (File sugar : neededPage) {
            String filePath = sugar.getCanonicalPath();
            String [] tokens = filePath.split ("\\\\");
            String userLogin = tokens [tokens.length - 2]; 
            String fileName = sugar.getName();
            
            String path = "../" + getServletContext().getInitParameter ("sugarsDirPath") + "/" + userLogin + "/" + fileName;
            
            out.println ("<table style='width : 900px; padding : 0px' cellpaddin='0' cellspacing='0'><tr><td style='width : 30%'>"
                    + "<img id='ordableSugar" + counter +"' style='width : 150px; height : 150px' src='" + path + "' />"
                    + "</td><td style='width : 70%; text-align : center'>"
                    + "<span id='ordableSugarLink" + counter + "' style='cursor : pointer; text-decoration : none; color : #999; font-size : 60px'>Order it</span>"
                    + "</td></tr></table><br />");
            
            counter++;
        }
    }
    
    private void printPager (int pagesAmount, PrintWriter out, HttpServletRequest request) {
        int pageNumber = new Integer (request.getParameter ("pageNumber"));
        
        out.println ("<center>");
        StringBuilder pager = new StringBuilder();
        for (int i = 0; i < pagesAmount; i++) {
            if (i == pageNumber) {
                pager.append ("&nbsp;&nbsp;<span style='font-weight : bold; text-decoration : underline;' id='page'>").append (i+1).append ("</span>&nbsp;&nbsp;");
            } else {
                pager.append ("&nbsp;&nbsp;<span id='page'>").append (i+1).append ("</span>&nbsp;&nbsp;");
            }
        }
        out.println (pager.toString() + "</center>");
    }
    
    @Override
    public String getServletInfo () {
        return "Short description";
    }

}