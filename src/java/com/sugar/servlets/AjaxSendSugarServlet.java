package com.sugar.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 *
 * @author LordNighton
 *
 */

public class AjaxSendSugarServlet extends HttpServlet {
   
    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    } 

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String path = request.getParameter ("src").substring (3);
            
            ServletContext context = request.getServletContext();
            String realPath = context.getRealPath (path);
            
            File sugarFile = new File (realPath).getCanonicalFile();
            if (true == sugarFile.exists ()) {
                sugarFile.delete();
                
                deleteOrder (sugarFile.getName());
            } 
        } finally { 
            out.close();
        }
    }

    @Override
    public String getServletInfo () {
        return "Short description";
    }

    private void deleteOrder (String orderFileName) {
        try {
            String correctOrderName = orderFileName.substring (0, orderFileName.length() - 4);
            
            String ordersDirPath = getServletContext().getInitParameter ("orders.folder");
            String root = getServletContext().getRealPath (ordersDirPath);
            File ordersRootDir = new File (root).getCanonicalFile();

            ArrayList <File> allOrders = new ArrayList <File> ((List <File>) FileUtils.listFiles (ordersRootDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE));
            
            for (File order : allOrders) {
                String orderName = order.getName ();
                if (true == orderName.contains (correctOrderName)) {
                    order.delete ();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}