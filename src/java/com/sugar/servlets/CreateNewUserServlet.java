package com.sugar.servlets;

import java.io.IOException;
import com.sugar.utils.UserUtils;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

public class CreateNewUserServlet extends HttpServlet {

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }
    
    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        String login = (String) request.getParameter ("login");
        String password = (String) request.getParameter ("password");
        
        if (false == login.isEmpty() && false == password.isEmpty ()) {
            if (false == UserUtils.isAdmin (login, password) && null == UserUtils.getUserByLogin (login)) {
                UserUtils.addUserToDB (login, password);
            }
        }
        
        response.sendRedirect ("protected/adminPanel.jsp");
    }

    @Override
    public String getServletInfo () {
        return "Short description";
    }
    
}