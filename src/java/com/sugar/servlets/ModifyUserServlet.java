package com.sugar.servlets;

import java.io.IOException;
import com.sugar.users.User;
import com.sugar.utils.UserUtils;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

public class ModifyUserServlet extends HttpServlet {

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }
    
     protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8");
        
        String userLogin = (String) request.getParameter ("modifiedUserLogin");
        String newPassword = (String) request.getParameter ("password");
        
        
        if (false == UserUtils.isAdmin (userLogin, newPassword)) {
            User user = UserUtils.getUserByLogin (userLogin);
            
            if (null != user && false == user.getPassword().equals (newPassword)) {
                UserUtils.changeUserPassword (userLogin, newPassword);
            } 
        }
        response.sendRedirect ("protected/adminPanel.jsp");
    }

    @Override
    public String getServletInfo () {
        return "Short description";
    }
    
}