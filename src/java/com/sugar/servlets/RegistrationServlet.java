package com.sugar.servlets;

import java.io.File;
import java.util.List;
import java.util.Iterator;
import nl.captcha.Captcha;
import java.io.IOException;
import com.sugar.users.User;
import com.sugar.users.UserImpl;
import com.sugar.utils.UserUtils;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(name = "RegistrationServlet", urlPatterns = {"/RegistrationServlet"})
public class RegistrationServlet extends HttpServlet {

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }
    
    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        parseParameters (request, response);
    }
    
    private void parseParameters (HttpServletRequest request, HttpServletResponse response) {
        boolean isMultipart = ServletFileUpload.isMultipartContent (request);
        if (true == isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload (factory);

            String login = null;
            String password = null;
            String answer = null;
            FileItem avatarFileItem = null;
            
            try {
                List items = upload.parseRequest (request);
                Iterator iterator = items.iterator();
                while (true == iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();

                    if (true == item.isFormField()) {
                        if (true == "login".equals (item.getFieldName())) {
                            login = item.getString();
                        }
                        if (true == "password".equals (item.getFieldName())) {
                            password = item.getString();
                        }
                        if (true == "answer".equals (item.getFieldName())) {
                            answer = item.getString();
                        }
                    } else {
                        avatarFileItem = item;
                    }
                }
                
                if (true == isCaptchaCorrect (request, answer)) {
                    User user = (User) request.getSession().getAttribute ("currentUser");
                    if (true == UserUtils.isAdmin (login, password)) {
                        user.setLogin (login);
                        user.setPassword (password);

                        response.sendRedirect ("protected/index.jsp");
                    } else { 
                        final User dbUser = UserUtils.getUserByLogin (login);
                        if (null == dbUser) {
                            uploadAvatar (avatarFileItem, login);
                            
                            UserImpl newUser = new UserImpl();
                            newUser.setLogin (login);
                            newUser.setPassword (password);

                            request.getSession().setAttribute ("currentUser", newUser);

                            UserUtils.addUserToDB (login, password);
                        } else {
                            if (true == dbUser.getLogin().equals (login) && true == dbUser.getPassword().equals (password)) {
                                if (true == user.isUserUnloginned ()) {
                                    user.setLogin (login);
                                    user.setPassword (password);
                                }
                            }
                        }
                        response.sendRedirect ("protected/index.jsp");
                    }
                } else {
                    response.sendRedirect ("protected/registration.jsp");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private boolean isCaptchaCorrect (HttpServletRequest request, String answer) {
        Captcha captcha = (Captcha) request.getSession().getAttribute (Captcha.NAME);

        return captcha.isCorrect (answer);
    }

    private void uploadAvatar (FileItem avatarFile, String login) {
        try {
            String [] tokens = avatarFile.getName().split("\\.");
            String fileName = login + "." + tokens [1];
            
            String avatarsDirPath = getServletContext().getInitParameter ("avatarsDirPath");
            String root = getServletContext().getRealPath (avatarsDirPath);

            File avatarsDir = new File (root).getCanonicalFile();
            if (false == avatarsDir.exists()) {
                avatarsDir.mkdirs();
            }

            File uploadedFile = new File (avatarsDir + "/" + fileName);
            avatarFile.write (uploadedFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Registration Servlet";
    }
    
}