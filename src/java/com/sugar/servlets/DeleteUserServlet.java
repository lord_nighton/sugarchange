package com.sugar.servlets;

import java.sql.Connection;
import java.io.IOException;
import com.sugar.utils.UserUtils;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(name = "DeleteUserServlet", urlPatterns = {"/DeleteUserServlet"})
public class DeleteUserServlet extends HttpServlet {

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        
        UserUtils.deleteUserByLogin ((String) request.getParameter ("delete"));
        
        response.sendRedirect ("protected/adminPanel.jsp");
    }
    
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo() {
        return "Delete user servlet";
    }
    
}