package com.sugar.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 *
 * @author LordNighton
 *
 */

@WebServlet(name="AjaxDisplayAllOrdersServlet", urlPatterns={"/AjaxDisplayAllOrdersServlet"})
public class AjaxDisplayAllOrdersServlet extends HttpServlet {
   
    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    } 

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            ArrayList <File> orders = defineAllOrders ();
            if (0 == orders.size()) {
                out.println ("<center><span style='font-size : 20px'>Sorry, there are no orders at system now</span><br />Please, order sugars before</center>");
            } else {
                printOrders (orders, out);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally { 
            out.close();
        }
    }

    private ArrayList<File> defineAllOrders() throws IOException {
        String ordersDirPath = getServletContext().getInitParameter ("orders.folder");
        String root = getServletContext().getRealPath (ordersDirPath);
        File ordersRootDir = new File (root).getCanonicalFile();
            
        return new ArrayList <File> ((List <File>) FileUtils.listFiles (ordersRootDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE));
    }

    private void printOrders (ArrayList<File> orders, PrintWriter out) {
        out.println ("<b><table style='width : 900px; text-align : center'><tr><td style='text-align : center; width : 300px'>Sender</td><td style='text-align : center; width : 300px'>Sugar</td><td style='text-align : center; width : 300px'>Receiver</td></tr>");
        for (File order : orders) {
            String fileName = order.getName();
            
            String [] tokens = fileName.split ("_");
            
            String receiver = tokens [0];
            String sender = tokens [1];
            
            String [] fileTokens = tokens[2].split("#");
            String sugarFileName = fileTokens[0] + "." + fileTokens[1];
            
            String sugarPath = "../sugars/" + tokens[1] + "/" + sugarFileName.substring (0, sugarFileName.length() - 4);
            
            out.println ("<tr><td>" + sender + "</td><td><img style='width : 150px; height : 150px;' src='" + sugarPath + "' /></td><td>" + receiver + "</td></tr>");
        }
        out.println ("</table></b>");
    }
    
    @Override
    public String getServletInfo () {
        return "Short description";
    }
    
}