package com.sugar.encoders;

import java.security.MessageDigest;

/**
 *
 * @author LordNighton
 *
 */

public class MDFiveEncoder {
    
    public static String transformUsingMDFive (String straighSequence) throws Exception {
        MessageDigest md = MessageDigest.getInstance ("MD5");
        md.update (straighSequence.getBytes());

        byte byteData[] = md.digest();

        StringBuilder encodedSequence = new StringBuilder ();
        for (int i = 0; i < byteData.length; i++) {
            encodedSequence.append (Integer.toString ((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return encodedSequence.toString();
    }
    
}